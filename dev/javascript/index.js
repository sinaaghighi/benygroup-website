// Import Vue.js framework
import '../../lib/javascript/vue.global';

// <nav> VueApp
const navbarLinks = {
    data() {
        return {
            home: "Home",
            about: "About Us",
            contact: "Contact Us"
        }
    }
}
Vue.createApp(navbarLinks).mount('nav')

// <header> VueApp
const headerContent = {
    data() {
        return {
            welcome: "Welcome to",
            name: "BENY",
            description: "Group"
        }
    }
}
Vue.createApp(headerContent).mount('header')

// <main> VueApp
const aboutContent = {
    data() {
        return {
            title: "About Us"
        }
    }
}
Vue.createApp(aboutContent).mount('main')

// <section> VueApp
const contactUs = {
    data() {
        return {
            title: "Contact Us",
            mail: "mail@benygroup.ir",
            description: "Scan this code to see Contact Information!"
        }
    }
}
Vue.createApp(contactUs).mount('section')