const path = require('path');

module.exports = {
    entry: {
        index: "./dev/javascript/index.js"
    },
    output: {
        filename: "index.js",
        path: __dirname + "/dist/javascript"
    }
}