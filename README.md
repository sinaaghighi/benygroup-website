# Requirements to contribute this project

To contribute this project, You need to know about some technologies. Here we listed these technologies:

- HTML5 & CSS3
- Vue, JavaScript framework
- npm (Node Package Manager)
- SASS Language
- Webpack

# Project Directory Structure

- The output directory for CSS files is /dist/stylesheet
- The output directory for JS files is /dist/javascript
- CSS and JavaScript Libraries are imported from /lib directory
- You've to use SASS to watch & export the changes from '/dev/stylesheet' to output directory.
- You've to use Webpack to watch and export the changes from 'dev/javascript/ to output directory.
- The directory '/node_modules' is ignored, So you've to install whatever you need to contribute.
